#!/usr/bin/python
import psutil as psutil


def getTemperture():
    tFile = open('/sys/class/thermal/thermal_zone0/temp')
    temp = float(tFile.read())
    tempC = temp / 1000
    tFile.close()
    return tempC


def getMemmory():
    ram = psutil.virtual_memory()
    ram_total = ram.total / 2 ** 20  # MiB.
    ram_used = ram.used / 2 ** 20
    return ram_used

def getNetwork():
    net = psutil.net_io_counters()
    net_send = net.bytes_sent
    return net_send

# while True:
#     print(getTemperture())
#     print(getMemmory())
