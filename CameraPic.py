import datetime
import requests

def getPicture():
    url = 'http://10.0.0.212:8080/?action=snapshot'
    now = datetime.datetime.now()
    fileName ='picture/' + str(now.year)+str(now.month)+str(now.day)+'_'+str(now.hour)+str(now.minute)+str(now.second)+ '_image.png'
    req = requests.get(url)
    file = open(fileName, 'wb')
    for chunk in req.iter_content(100000):
        file.write(chunk)
    file.close()
