#!/usr/bin/env python
# coding: latin-1


# Es werden verschiedene Python Klassen importiert deren Funktionen
# im Programm benoetigt werden fuer die Programmverarbeitung.
import sys, tty, termios, os, readchar

# Das Programm L298NHBridge.py wird als Modul geladen. Es stellt
# die Funktionen fuer die Steuerung der H-Bruecke zur Verfuegung.
import drv as drv

# Das Programm Ultra.py wird als Modul geladen. Es stellt
# die Funktionen zur Ermittlung der Abst�nde bereit.
import Ultra as Ultra

#CameraPic.py wird geladen damit Bilder gespeichert werden k�nnnen.
import CameraPic as CameraPic

# Das Menue fuer den Anwender wenn er das Programm ausfuehrt.
# Das Menue erklaert mit welchen Tasten das Auto gesteuert wird.
print("w/s: beschleunigen")
print("a/d: lenken")
print("q: stoppt die Motoren")
print("x: Programm beenden")
print("p: Bild aufnehmen")
print("============ Abstandsanzeige ================")
Ultra.DISTANZ()
print("============== Koordinaten ==================")
drv.WHICH_DIR()


# print("Abstand links: ", Ultra.distanz_l())
# print("Abstand vorne: ", Ultra.distanz_v())
# print("Abstand rechts: ", Ultra.distanz_r())


# Die Funktion getch() nimmt die Tastatureingabe des Anwenders
# entgegen. Die gedrueckten Buchstaben werden eingelesen. Sie werden
# benoetigt um die Richtung und Geschwindigkeit des Roboter-Autos
# festlegen zu koennen.
def getch():
    ch = readchar.readchar()
    return ch


# Die Funktion printscreen() gibt immer das aktuelle Menue aus
# sowie die Geschwindigkeit der linken und rechten Motoren wenn
# es aufgerufen wird.
def printscreen():
    # der Befehl os.system('clear') leert den Bildschirmihalt vor
    # jeder Aktualisierung der Anzeige. So bleibt das Menue stehen
    # und die Bildschirmanzeige im Terminal Fenster steht still.
    os.system('clear')
    print("w/s: beschleunigen")
    print("a/d: lenken")
    print("q:   stoppt die Motoren")
    print("x:   Programm beenden")
    print("============ Abstandsanzeige ================")
    Ultra.DISTANZ()
    print("============== Koordinaten ==================")
    drv.WHICH_DIR()



#   print ("Abstand links: ", Ultra.distanz_l())
#   print ("Abstand vorne: ", Ultra.distanz_v())
#   print ("Abstand rechts: ", Ultra.distanz_r())

# Diese Endlosschleife wird erst dann beendet wenn der Anwender 
# die Taste X tippt. Solange das Programm laeuft wird ueber diese
# Schleife die Eingabe der Tastatur eingelesen.
while True:
    # Mit dem Aufruf der Funktion getch() wird die Tastatureingabe
    # des Anwenders eingelesen. Die Funktion getch() liesst den
    # gedrueckte Buchstabe ein und uebergibt diesen an die
    # Variablechar. So kann mit der Variable char weiter
    # gearbeitet werden.
    char = getch()

    # Das Roboter-Auto faehrt vorwaerts wenn der Anwender die
    # Taste "w" drueckt.
    if (char == "w"):
        print("STRAIGHT")
        drv.STRAIGHT()
        printscreen()

    #     # Das Roboter-Auto stoppt wenn die Taste "s"
    #     # gedrueckt wird.
    #     if (char == "s"):
    #         # drv8825.STOP()
    #         printscreen()
    #
    #     # mit dem druecken der Taste "q" werden die Motoren angehalten
    #     if (char == "q"):
    #         # speedleft = 0
    #         # speedright = 0
    #         printscreen()
    #
    #     # Mit der Taste "d" lenkt das Auto nach rechts bis die max/min
    #     # Geschwindigkeit der linken und rechten Motoren erreicht ist.
    if (char == "d"):
        print("RIGHT_TURN")
        drv.RIGHT_TURN()
        printscreen()

    # Mit der Taste "a" lenkt das Auto nach links bis die max/min
    # Geschwindigkeit der linken und rechten Motoren erreicht ist.
    if (char == "a"):
        print("LEFT_TURN")
        drv.LEFT_TURN()
        printscreen()

    if (char == "p"):
        print("Bild aufnehmen")
        CameraPic.getPicture()
        printscreen()

    # Mit der Taste "x" wird die Endlosschleife beendet
    # und das Programm wird ebenfalls beendet. Zum Schluss wird
    # noch die Funktion exit() aufgerufen die die Motoren stoppt.
    if (char == "x"):
        drv.SHUTDOWN()
        print("Program Ended")
        break

    # Die Variable char wird pro Schleifendurchlauf geleert.
    # Das ist notwendig um weitere Eingaben sauber zu übernehmen.
    char = ""

# Ende des Programmes
