#!/usr/bin/env python
# coding: latin-1


# Es werden verschiedene Python Klassen importiert deren Funktionen
# im Programm benoetigt werden fuer die Programmverarbeitung.
import sys, tty, termios, os, readchar

# Das Programm L298NHBridge.py wird als Modul geladen. Es stellt
# die Funktionen fuer die Steuerung der H-Bruecke zur Verfuegung.

from datetime import datetime

import drv as drv
import i2c as i2c
import sendData as sendData
import CPU as CPU
import CameraPic as CameraPic


# Das Programm Ultra.py wird als Modul geladen. Es stellt
# die Funktionen zur Ermittlung der Abst�nde bereit.
import Ultra as Ultra

# maximale Werte f�r X und y
max_X = 600
max_Y = 600

# Ziel erreicht oder nicht
goal = True
# Koordinaten f�r das Ziel (x/y)
goal_X = 0
goal_Y = 0


# Das Menue fuer den Anwender wenn er das Programm ausfuehrt.
# Das Menue erklaert mit welchen Tasten das Auto gesteuert wird.
print("w/s: beschleunigen")
print("a/d: lenken")
print("q: stoppt die Motoren")
print("x: Programm beenden")
print("============ Abstandsanzeige ================")
Ultra.DISTANZ()
print("============== Koordinaten ==================")
drv.WHICH_DIR()

# Die Funktion getch() nimmt die Tastatureingabe des Anwenders
# entgegen. Die gedrueckten Buchstaben werden eingelesen. Sie werden
# benoetigt um die Richtung und Geschwindigkeit des Roboter-Autos
# festlegen zu koennen.
def getch():
    ch = readchar.readchar()
    return ch


# Die Funktion printscreen() gibt immer das aktuelle Menue aus
# sowie die Geschwindigkeit der linken und rechten Motoren wenn
# es aufgerufen wird.
def printscreen():
    # der Befehl os.system('clear') leert den Bildschirmihalt vor
    # jeder Aktualisierung der Anzeige. So bleibt das Menue stehen
    # und die Bildschirmanzeige im Terminal Fenster steht still.
    os.system('clear')
    print("x:   Programm beenden")
    print("============ Abstandsanzeige ================")
    Ultra.DISTANZ()
    print("============== Koordinaten ==================")
    drv.WHICH_DIR()


# Diese Endlosschleife wird erst dann beendet wenn der Anwender 
# die Taste X tippt. Solange das Programm laeuft wird ueber diese
# Schleife die Eingabe der Tastatur eingelesen.
while goal:
    if goal_X == 0 and goal_Y == 0:
        print("bitte die Zielkoordinaten eingeben, ein Feld ist 150 x 150, Bsp. 150/300 -> Feld mit X = 1 und Y = 2 ")
        goal_X = int(input("X Eingeben:"))
        goal_Y = int(input("Y Eingeben:"))

    printscreen()

    #werte von Arduino lesen
    values = i2c.readValues()
    # wert auf ThingSpeak hochladen
    sendData.sendArray(values)
    #werte in eine Datei schreiben
    with open("data.csv", 'a') as out:
        out.write(str(datetime.now()) + " " + str(values) +"; Mem " + str(CPU.getMemmory())+"; Temp "+ str(CPU.getTemperture()) + " ; Byte_send " + str(CPU.getNetwork()) + " ; X " + str(drv.X_VALUE) + " ; Y " + str(drv.Y_VALUE) + '\n')

    #get Pic from cam
    CameraPic.getPicture()

    # Die Variable char wird pro Durchlauf geleert
    char = ""

    # Mit der Taste "x" wird die Endlosschleife beendet
    # und das Programm wird ebenfalls beendet. Zum Schluss wird
    # noch die Funktion exit() aufgerufen die die Motoren stoppt.
    if (char == "x"):
        drv.SHUTDOWN()
        print("Program Ended")
        break



    distanz_v = (Ultra.distanz_vl() + Ultra.distanz_vr()) / 2
    distanz_l = Ultra.distanz_l()
    distanz_r = Ultra.distanz_r()

    if drv.X_VALUE == goal_X and drv.Y_VALUE == goal_Y:
        print("reached goal :-)")
        goal = False

    elif drv.DIRECTION == 1 and drv.Y_VALUE < goal_Y and drv.Y_VALUE < max_Y :
        print("dir1_1")
        if distanz_v > 40:
            drv.STRAIGHT()
        elif distanz_l > 40 and distanz_v < 40:
            drv.LEFT_TURN()
        elif distanz_r > 40 and distanz_l < 40 and distanz_v < 40:
            drv.RIGHT_TURN()

    elif drv.DIRECTION == 1 and drv.Y_VALUE == goal_Y and drv.X_VALUE < goal_X and drv.X_VALUE < max_X:
        print("dir1_2")
        if distanz_l > 40:
            drv.LEFT_TURN()
        elif distanz_r > 40 and drv.X_VALUE > 0:
            drv.RIGHT_TURN()

    elif drv.DIRECTION == 2 and drv.X_VALUE < goal_X and drv.X_VALUE < max_X:
        print("dir2_1")
        if distanz_v > 40:
            drv.STRAIGHT()
        elif distanz_r > 40 and distanz_v < 40:
            drv.RIGHT_TURN()
        elif distanz_r < 40 and distanz_v < 40  and distanz_l > 40:
            drv.LEFT_TURN()
        elif distanz_r < 40 and distanz_v < 40 and distanz_l < 40:
            drv.RIGHT_TURN()
            drv.RIGHT_TURN()

    elif drv.DIRECTION == 2 and drv.X_VALUE == goal_X and drv.X_VALUE < max_X:
        print("dir2_2")
        if distanz_r > 40:
            drv.RIGHT_TURN()
        elif distanz_l > 40 and distanz_r < 40:
            drv.LEFT_TURN()
        elif distanz_l < 40 and distanz_r < 40 and distanz_v < 40:
            drv.RIGHT_TURN()
            drv.RIGHT_TURN()

    elif drv.DIRECTION == 3 and drv.X_VALUE > 0 and drv.Y_VALUE > 0:
        print("dir3_1")
        if distanz_v > 40:
            drv.STRAIGHT()
        elif distanz_l > 40 and distanz_r < 40:
            drv.RIGHT_TURN()
        elif distanz_r > 40 and distanz_l < 40 and distanz_v < 40:
            drv.RIGHT_TURN()
        else:
            drv.RIGHT_TURN()
            drv.RIGHT_TURN()

    elif drv.DIRECTION == 4 and drv.X_VALUE > 0 and drv.Y_VALUE > 0:
        print("dir4_1")
        if distanz_v > 40:
            drv.STRAIGHT()
        elif distanz_l > 40 and distanz_r < 40:
            drv.RIGHT_TURN()
        elif distanz_r > 40 and distanz_l < 40 and distanz_v < 40:
            drv.RIGHT_TURN()
        else:
            drv.RIGHT_TURN()
            drv.RIGHT_TURN()


    else:
        drv.WHICH_DIR
        print("DIRECTION=1 -> keine Ahnung wohin ..... :-(")
        print("X ", drv.X_VALUE, " Y ", drv.Y_VALUE)
        break


# Ende des Programmes
