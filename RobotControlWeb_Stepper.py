#!/usr/bin/env python
# coding: latin-1

import sys
# Mit diesem Aufruf sucht Python im angegebenen Ordner nach 
# weiteren Modulen die importiert werden koennen.
sys.path.append("/home/pi/robot")

# Das Programm drv.py wird als Modul geladen. Es stellt
# die Funktionen fuer die Steuerung der  zur Verfuegung.
import drv as drv

#CameraPic laden
import CameraPic as CameraPic

# WebIOPI wird importiert damit die Funktionen der Web-Steuerung
# von WebIOPI verwendet werden koennen.
import webiopi

# Die Funktion "ButtonForward()" legt fest, dass das Roboter
# Auto geradeaus nach Vorne faehrt.
@webiopi.macro
def ButtonForward():
   drv.STRAIGHT()

# Die Funktion "ButtonReverse()" legt fest, dass das Roboter  
# Auto rueckwaerts faehrt.
@webiopi.macro
def ButtonReverse():   
   drv.REVERSE()

# Die Funktion "ButtonTurnLeft()" legt fest, dass das Roboter  
# Auto nach links faehrt.
@webiopi.macro
def ButtonTurnLeft():
   drv.LEFT_TURN()

# Die Funktion "ButtonTurnRight()" legt fest, dass das Roboter  
# Auto nach rechts faehrt.
@webiopi.macro
def ButtonTurnRight():   
   drv.RIGHT_TURN()

# Die Funktion "ButtonTurnRight()" legt fest, dass das Roboter
# Auto nach rechts faehrt.
@webiopi.macro
def ButtonStop():
   CameraPic.getPicture()


#zus�tzliche funktionen
@webiopi.macro
def ButtonForward20():
   drv.STRAIGHT20()

@webiopi.macro
def ButtonTurnRight45():
   drv.RIGHT_TURN45()

@webiopi.macro
def ButtonTurnLeft45():
   drv.LEFT_TURN45()



# Es wird der WebIOPi Web-Service des  WebIOPI Web-Servers
# auf dem Port 8000 gestartet. Ein Passwort wird nicht vergeben.
server = webiopi.Server(port=8000, coap_port=8081, configfile=None)

# Es muessen noch die Buttons die auf der Web-Oberflaeche 
# verwendet werden definiert werden. Dies geschieht im folgenden 
# Abschnitt. Diese hier definierten Button-Funktionen werden von
# der Web-Oberflaeche und dem dort hinterlegten Java-Skript
# aufgerufen und ermoeglichen so die Fernsteuerung des Roboter-Autos.
server.addMacro(ButtonForward)
server.addMacro(ButtonStop)
server.addMacro(ButtonReverse)
server.addMacro(ButtonTurnRight)
server.addMacro(ButtonTurnLeft)


# f�r die halben
server.addMacro(ButtonForward20)
server.addMacro(ButtonTurnRight45)
server.addMacro(ButtonTurnLeft45)

# WebIOPi wird in einer Endlos-Schleife gestartet damit die 
# Befehle jederzeit entgegengenommen werden koennen.
webiopi.runLoop()
server.stop()
   
# Ende des Programms
