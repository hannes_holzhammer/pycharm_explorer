import http.client, urllib.parse
import time

channel_id = 500129  # PUT CHANNEL ID HERE
write_key = 'ENUNHTN02KXLJ2T8'  # PUT YOUR WRITE KEY HERE
read_key = 'OUAP633S9THH85SY'  # PUT YOUR READ KEY HERE

def sendArray(arr):

    #Beschleunigung - arr[0] = mpuAX
    #Gyro - arr[3] = mpuGX
    #Magnetfeld - arr[6] = mpuMDirection
    #Feuchtigkeit - arr[10] = dhtHum
    #Temperatur - arr[11] = dhtTemp

    params = urllib.parse.urlencode({'field1': arr[0], 'field2': arr[3], 'field3': arr[6], 'field4': arr[10], 'field5': arr[11], 'key': write_key})
    headers = {"Content-typZZe": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    conn = http.client.HTTPConnection("api.thingspeak.com:80")

    try:
        conn.request("POST", "/update", params, headers)

        response = conn.getresponse()
        print("response: ")
        print(response.status, response.reason)
        data = response.read()
        conn.close()

    except:
        print("connection failed")

#if __name__ == "__main__":
#    array = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

#    while True:
#        sendArray(array)
        # free account has an api limit of 15sec
#       time.sleep(15)