#!/usr/bin/env python
# coding: latin-1


# Bibliotheken einbinden
import RPi.GPIO as IO
import time

# GPIO Modus (BCM)
IO.setmode(IO.BCM)

# GPIO Pins zuweisen
# Trigger der Ultraschallsensoren
GPIO_TRIGGER = 18

# ECHO Pin nach vorne links
GPIO_ECHO_1 = 23
# ECHO Pin nach vorne rechts
GPIO_ECHO_4 = 24
# ECHO Pin nach rechts
GPIO_ECHO_2 = 22
# ECHO Pin nach links
GPIO_ECHO_3 = 27

# Richtung der GPIO-Pins festlegen (IN / OUT)
IO.setup(GPIO_TRIGGER, IO.OUT)
IO.setup(GPIO_ECHO_1, IO.IN)
IO.setup(GPIO_ECHO_2, IO.IN)
IO.setup(GPIO_ECHO_3, IO.IN)
IO.setup(GPIO_ECHO_4, IO.IN)

#gibt die Distanz nach vorne links zur�ck
def distanz_vl():
    # setze Trigger auf HIGH
    IO.output(GPIO_TRIGGER, True)

    # setze Trigger nach 5ms auf LOW
    time.sleep(0.005)
    IO.output(GPIO_TRIGGER, False)

    StartZeit = time.time()
    StopZeit = time.time()

    # speichere Startzeit
    while IO.input(GPIO_ECHO_1) == 0:
        StartZeit = time.time()

    # speichere Ankunftszeit
    while IO.input(GPIO_ECHO_1) == 1:
        StopZeit = time.time()

    # Zeit Differenz zwischen Start und Ankunft
    TimeElapsed = StopZeit - StartZeit
    # mit der Schallgeschwindigkeit (34300 cm/s) multiplizieren
    # und durch 2 teilen, da hin und zurueck
    distanz = (TimeElapsed * 34300) / 2

    return distanz

#gibt die Distanz nach vorne rechts zur�ck
def distanz_vr():
    # setze Trigger auf HIGH
    IO.output(GPIO_TRIGGER, True)

    # setze Trigger nach 5ms auf LOW
    time.sleep(0.005)
    IO.output(GPIO_TRIGGER, False)

    StartZeit = time.time()
    StopZeit = time.time()

    # speichere Startzeit
    while IO.input(GPIO_ECHO_4) == 0:
        StartZeit = time.time()

    # speichere Ankunftszeit
    while IO.input(GPIO_ECHO_4) == 1:
        StopZeit = time.time()

    # Zeit Differenz zwischen Start und Ankunft
    TimeElapsed = StopZeit - StartZeit
    # mit der Schallgeschwindigkeit (34300 cm/s) multiplizieren
    # und durch 2 teilen, da hin und zurueck
    distanz = (TimeElapsed * 34300) / 2

    return distanz


# gibt  die Distanz nach rechts zur�ck
def distanz_r():
    # setze Trigger auf HIGH
    IO.output(GPIO_TRIGGER, True)

    # setze Trigger nach 5ms auf LOW
    time.sleep(0.005)
    IO.output(GPIO_TRIGGER, False)

    StartZeit = time.time()
    StopZeit = time.time()

    # speichere Startzeit
    while IO.input(GPIO_ECHO_2) == 0:
        StartZeit = time.time()

    # speichere Ankunftszeit
    while IO.input(GPIO_ECHO_2) == 1:
        StopZeit = time.time()

    # Zeit Differenz zwischen Start und Ankunft
    TimeElapsed = StopZeit - StartZeit
    # mit der Schallgeschwindigkeit (34300 cm/s) multiplizieren
    # und durch 2 teilen, da hin und zurueck
    distanz = (TimeElapsed * 34300) / 2

    return distanz


# gibt  die Distanz nach rechts zur�ck
def distanz_l():
    # setze Trigger auf HIGH
    IO.output(GPIO_TRIGGER, True)

    # setze Trigger nach 5ms auf LOW
    time.sleep(0.005)
    IO.output(GPIO_TRIGGER, False)

    StartZeit = time.time()
    StopZeit = time.time()

    # speichere Startzeit
    while IO.input(GPIO_ECHO_3) == 0:
        StartZeit = time.time()

    # speichere Ankunftszeit
    while IO.input(GPIO_ECHO_3) == 1:
        StopZeit = time.time()

    # Zeit Differenz zwischen Start und Ankunft
    TimeElapsed = StopZeit - StartZeit
    # mit der Schallgeschwindigkeit (34300 cm/s) multiplizieren
    # und durch 2 teilen, da hin und zurueck
    distanz = (TimeElapsed * 34300) / 2

    return distanz

# Ausgabe aller Distanzen
def DISTANZ():
    print("LINKS: ", distanz_l(), " RECHTS: ", distanz_r(), " VORNE: ", (distanz_vl() + distanz_vr()) / 2)

def exit():
   IO.output(GPIO_ECHO_1, False)
   IO.output(GPIO_ECHO_2, False)
   IO.output(GPIO_ECHO_3, False)
   IO.output(GPIO_TRIGGER, False)
   IO.cleanup()
