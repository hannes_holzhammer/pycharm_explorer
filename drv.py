from time import sleep
import RPi.GPIO as GPIO

DIR_L = 20  # Direction GPIO Pin LEFT
STEP_L = 21  # Step GPIO Pin LEFT
DIR_R = 7  # Direction GPIO Pin LEFT
STEP_R = 8  # Step GPIO Pin LEFT

CW = 1  # Clockwise Rotation
CCW = 0  # Counterclockwise Rotation

SPR = 307  # 200 Steps per Rotation (360 / 1.8) -> 307 Steps = 30 cm
SPHR = 240 # Steps per half rotation
SPHL = 240 # Steps per half rotation

#global vars for position
X_VALUE = 0
Y_VALUE = 0

# Var to know in which direction the car is driving
# direction of begin is 1 -> Y
DIRECTION = 1
# if 1 ... Y
# if 2 ... X
# if 3 ... -X
# if 4 .... -Y


GPIO.setmode(GPIO.BCM)
GPIO.setup(DIR_L, GPIO.OUT)
GPIO.setup(STEP_L, GPIO.OUT)
GPIO.output(DIR_L, CW)

GPIO.setup(DIR_R, GPIO.OUT)
GPIO.setup(STEP_R, GPIO.OUT)

#delay between steps
delay = .008

# umrechnung in cm
stepsize = 30

def WHICH_DIR():
    global X_VALUE
    global Y_VALUE
    global DIRECTION

    print("X: ",X_VALUE," Y: ",Y_VALUE," DIRECTION: ",DIRECTION)

def ADD_DIR_STR():
    global X_VALUE
    global Y_VALUE
    global DIRECTION

    if DIRECTION == 1:
        Y_VALUE = Y_VALUE + stepsize
        DIRECTION = DIRECTION
    elif DIRECTION == 2:
        X_VALUE = X_VALUE + stepsize
        DIRECTION = DIRECTION
    elif DIRECTION == 3:
        X_VALUE = X_VALUE - stepsize
        DIRECTION = DIRECTION
    elif DIRECTION == 4:
        Y_VALUE = Y_VALUE - stepsize
        DIRECTION = DIRECTION


def ADD_DIR_RIGHT():
    global X_VALUE
    global Y_VALUE
    global DIRECTION

    if DIRECTION == 1:
        DIRECTION = 3
    elif DIRECTION == 2:
        DIRECTION = 1
    elif DIRECTION == 3:
        DIRECTION = 4
    elif DIRECTION == 4:
        DIRECTION = 2

def ADD_DIR_LEFT():
    global X_VALUE
    global Y_VALUE
    global DIRECTION

    if DIRECTION == 1:
        DIRECTION = 2
    elif DIRECTION == 2:
        DIRECTION = 4
    elif DIRECTION == 3:
        DIRECTION = 1
    elif DIRECTION == 4:
        DIRECTION = 3

def RIGHT_TURN():

    step_count = SPHR
    GPIO.output(DIR_L, CCW)
    GPIO.output(DIR_R, CCW)

    for x in range(step_count):
        # both sides high
        GPIO.output(STEP_L, GPIO.HIGH)
        GPIO.output(STEP_R, GPIO.HIGH)
        sleep(delay)
        # both sides low
        GPIO.output(STEP_L, GPIO.LOW)
        GPIO.output(STEP_R, GPIO.LOW)
        sleep(delay)

    ADD_DIR_RIGHT()
    sleep(.5)


def LEFT_TURN():

    step_count = SPHL
    GPIO.output(DIR_L, CW)
    GPIO.output(DIR_R, CW)

    for x in range(step_count):
        # both sides high
        GPIO.output(STEP_L, GPIO.HIGH)
        GPIO.output(STEP_R, GPIO.HIGH)
        sleep(delay)
        # both sides low
        GPIO.output(STEP_L, GPIO.LOW)
        GPIO.output(STEP_R, GPIO.LOW)
        sleep(delay)

    ADD_DIR_LEFT()
    sleep(.5)

def RIGHT_TURN_CORR():

    step_count = 2
    GPIO.output(DIR_L, CCW)
    GPIO.output(DIR_R, CCW)

    for x in range(step_count):
        # both sides high
        GPIO.output(STEP_L, GPIO.HIGH)
        GPIO.output(STEP_R, GPIO.HIGH)
        sleep(delay)
        # both sides low
        GPIO.output(STEP_L, GPIO.LOW)
        GPIO.output(STEP_R, GPIO.LOW)
        sleep(delay)

    sleep(.5)



def RIGHT_TURN45():

    step_count = SPHR/2
    GPIO.output(DIR_L, CCW)
    GPIO.output(DIR_R, CCW)

    for x in range(step_count):
        # both sides high
        GPIO.output(STEP_L, GPIO.HIGH)
        GPIO.output(STEP_R, GPIO.HIGH)
        sleep(delay)
        # both sides low
        GPIO.output(STEP_L, GPIO.LOW)
        GPIO.output(STEP_R, GPIO.LOW)
        sleep(delay)

    ADD_DIR_RIGHT()
    sleep(.5)


def LEFT_TURN45():

    step_count = SPHL/2
    GPIO.output(DIR_L, CW)
    GPIO.output(DIR_R, CW)

    for x in range(step_count):
        # both sides high
        GPIO.output(STEP_L, GPIO.HIGH)
        GPIO.output(STEP_R, GPIO.HIGH)
        sleep(delay)
        # both sides low
        GPIO.output(STEP_L, GPIO.LOW)
        GPIO.output(STEP_R, GPIO.LOW)
        sleep(delay)

    ADD_DIR_LEFT()
    sleep(.5)

def STRAIGHT20():
    step_count = 200
    GPIO.output(DIR_L, CCW)
    GPIO.output(DIR_R, CW)

    for x in range(step_count):
        #both sides high
        GPIO.output(STEP_L, GPIO.HIGH)
        GPIO.output(STEP_R, GPIO.HIGH)
        sleep(delay)
        # both sides low
        GPIO.output(STEP_R, GPIO.LOW)
        GPIO.output(STEP_L, GPIO.LOW)
        sleep(delay)

    ADD_DIR_STR()
    RIGHT_TURN_CORR()
    sleep(.5)

def STRAIGHT():
    step_count = SPR
    GPIO.output(DIR_L, CCW)
    GPIO.output(DIR_R, CW)

    for x in range(step_count):
        #both sides high
        GPIO.output(STEP_L, GPIO.HIGH)
        GPIO.output(STEP_R, GPIO.HIGH)
        sleep(delay)
        # both sides low
        GPIO.output(STEP_R, GPIO.LOW)
        GPIO.output(STEP_L, GPIO.LOW)
        sleep(delay)

    ADD_DIR_STR()
    RIGHT_TURN_CORR()
    sleep(.5)

def REVERSE():
    step_count = SPR
    GPIO.output(DIR_L, CW)
    GPIO.output(DIR_R, CCW)

    for x in range(step_count):
        # both sides high
        GPIO.output(STEP_L, GPIO.HIGH)
        GPIO.output(STEP_R, GPIO.HIGH)
        sleep(delay)
        # both sides low
        GPIO.output(STEP_L, GPIO.LOW)
        GPIO.output(STEP_R, GPIO.LOW)
        sleep(delay)

    sleep(.5)


def SHUTDOWN():
    GPIO.cleanup()
