import smbus
import time

bus = smbus.SMBus(1)

#addess as defined on Arduino
address = 0x04

def writeNumber(value):
    bus.write_byte(address,value)
    return -1

def readNumber():
    numArr = bus.read_i2c_block_data(0x04, 0xFF, 12)
    time.sleep(1)
    numArr = bus.read_i2c_block_data(0x04, 0xFF, 12)
    return numArr


def readValues():
    writeNumber(1)
    values = readNumber()
    return values



 # while True:
 #     var = int(input("1 for values: "))
 #     if not var:
 #         continue
 #
 #     writeNumber(var)
 #     print("entered number: ", var)
 #     time.sleep(1)
 #
 #     number = readNumber()
 #     if var == 1:
 #         print(number[0])
 #         print(number[1])
 #         print(number[2])
 #         print(number[3])
 #         print(number[4])
 #         print(number[5])
 #         print(number[6])
 #         print(number[7])
 #         print(number[8])
 #         print(number[9])
 #         print(number[10])
 #         print(number[11])
 #
 #     print
